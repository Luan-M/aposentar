package domain;

import java.util.Scanner;

public class Aposentar {
	
	static final Integer IDADE_MINIMA_HOMEM = 67;
	static final Integer TEMPO_MININO_HOMEM = 36;
	
	static final Integer IDADE_MINIMA_MULHER = 65;
	static final Integer TEMPO_MINIMO_MULHER = 34;
	
	static final Integer ANO_ATUAL = 2023;
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Vamos verificar se você já pode se aposentar.");
		
		System.out.println();
		
		System.out.println("Escolha uma opção.");
		System.out.print("1 - Começar | 2 - Informações para se aposentar: ");
		Integer menuInicio = scanner.nextInt();
		
		System.out.print("Digite o seu nome: ");
		String nomeUsuario = scanner.next();
		
		System.out.println("Escolha o seu sexo.");
		System.out.print("1 - Masculino | 2 - Feminino: ");
		Integer menuSexo = scanner.nextInt();
		
		System.out.print("Digite o ano que você nasceu: ");
		Integer anoUsuario = scanner.nextInt();
		
		Integer idadeUsuario = ANO_ATUAL - anoUsuario;
		
		System.out.print("Digite o tempo/ano de registrado na sua carteira de trabalho: ");
		Integer tempoUsuario = scanner.nextInt();
		
		if(menuInicio.equals(1)) {
			if(menuSexo.equals(1)) {
				Boolean homemAposentaIdade = idadeUsuario >= IDADE_MINIMA_HOMEM;
				Boolean homemAposentaTempo = tempoUsuario >= TEMPO_MININO_HOMEM;
				Boolean homemPodeAposentar = homemAposentaIdade && homemAposentaTempo;
				
				if(homemPodeAposentar) {
					System.out.println(nomeUsuario + " o senhor já pode se aposentar.");
				} else {
					System.out.println(nomeUsuario + " o senhor não pode se aposentar ainda.");
				}
				
			} else if(menuSexo.equals(2)) {
				Boolean mulherAposentaIdade = idadeUsuario >= IDADE_MINIMA_MULHER;
				Boolean mulherAposentaTempo = tempoUsuario >= TEMPO_MINIMO_MULHER;
				Boolean mulherPodeAposentar = mulherAposentaIdade && mulherAposentaTempo;
				
				if(mulherPodeAposentar) {
					System.out.println(nomeUsuario + " a senhora já pode se aposentar.");
				} else {
					System.out.println(nomeUsuario + " a senhora não pode se aposentar ainda.");
				}
			} else {
				System.out.println("Você tem que escolher 1 para masculino e 2 para feminino.");
			}
		} else if(menuInicio.equals(2)) {
			System.out.println();
			System.out.println("Informações");
			System.out.println("Homens: precisa ter no mínimo 67 anos e 36 anos de registro na carteira.");
			System.out.println("Mulheres: precisa ter no mínimo 65 anos e 34 anos de registro na carteira.");
		} else {
			System.out.println("Você só pode escolher");
			System.out.println("1 para começar ou 2 para saber as informações do que é preciso para se aposentar.");
		}
		
		scanner.close();
	}

}
