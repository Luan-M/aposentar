Vamos descobrir se já podemos aposentar.
Para isso precisamos saber se a pessoa é homem ou mulher.
Homem: precisa ter no mínimo 67 anos e ter trabalhado de carteira assinada por 36 anos.
Mulher: precisa ter no mínimo 65 anos e ter trabalhado de carteira assinada por 34 anos.
Atingindo os dois requisitos para o homem e para mulher, podem se aposentar.
